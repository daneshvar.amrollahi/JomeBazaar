An online shop written with C++/HTML/CSS

AI: Detecting inappropriate comments using Naive Bayes algorithm. 

Read the documents in the Description folder for more info.

---------------------------------------------------------------

How to Run:

1- enter 'make' command in terminal

2- enter localhost:5000 in your browser address bar

Contact me for more info.
---------------------------------------------------------------